#include"AgendaService.h"
#include<iostream>
AgendaService::AgendaService() {
	storage_ = Storage::getInstance();
}
bool AgendaService::userRegister(std::string userName, std::string password, std::string email, std::string phone) {
  std::function<bool(const User&)> filter = [userName](const User& u){return (u.getName() == userName);};
  if ((storage_->queryUser(filter)).empty()) {
    User reg(userName, password, email, phone);
    storage_->createUser(reg);
    return true;
  }
  return false;
}
bool AgendaService::userLogIn(std::string userName, std::string password) {
  std::function<bool(const User& u)> filter = [userName, password](const User& u)->bool{return (u.getName() == userName && u.getPassword() == password);};
  if (storage_->queryUser(filter).empty()) {
    return false;
  } else {
    return true;
  }
}
bool AgendaService::deleteUser(std::string userName, std::string password) {
  std::function<bool(const User& u)> filter = [userName, password](const User& u)->bool{return (u.getName() == userName && u.getPassword() == password);};
  if (storage_->queryUser(filter).empty()) return false;
  deleteAllMeetings(userName);
  storage_->deleteUser(filter);
  return true;
}
std::list<User> AgendaService::listAllUsers(void) {
  std::function<bool(const User& u)> filterx = [](const User& u)->bool{return true;};
  return (storage_->queryUser(filterx));
}
bool AgendaService::createMeeting(std::string userName, std::string title, std::string participator, std::string startDate, std::string endDate) {
  std::function<bool(const User& u)> filter = [participator](const User& u)->bool{return (u.getName() == participator);};
  if (storage_->queryUser(filter).empty()) return false;
  Date sd = Date::stringToDate(startDate);
  Date ed = Date::stringToDate(endDate);
  if (!(Date::isValid(sd) && Date::isValid(ed))) return false;
  if (sd >= ed) return false;
  std::function<bool(const Meeting& me1)> m1 = [userName, title](const Meeting& me1)->bool {
              if (userName == me1.getParticipator() || userName == me1.getSponsor()) {
                if (me1.getTitle() == title) return true;
              }
              return false;};
  if (!(storage_ -> queryMeeting(m1)).empty()) return false;
  std::function<bool(const Meeting& me2)> m2 = [participator, title](const Meeting& me2)->bool {
              if (participator == me2.getParticipator() || participator == me2.getSponsor()) {
                if (me2.getTitle() == title) return true;
              }
              return false;};
  if (!(storage_ -> queryMeeting(m1)).empty()) return false;
  std::function<bool(const Meeting& me)> m = [userName, participator] (const Meeting& me)->bool {
             if (me.getSponsor() == userName || me.getSponsor() == participator
                 || me.getParticipator() == userName || me.getParticipator() == participator) {
                return true;
             }
             return false;
  };
  std::list<Meeting> meet = storage_ -> queryMeeting(m);
  for (auto i = meet.begin(); i!= meet.end(); i++) {
                if ((sd <= i -> getStartDate() && ed <= i -> getStartDate()) || (sd >= i -> getEndDate() && ed >= i -> getEndDate())) {}
                else {return false;}
  }
  Meeting meeting(userName, participator, Date::stringToDate(startDate), Date::stringToDate(endDate), title);
  storage_->createMeeting(meeting);
  return true;
}
std::list<Meeting> AgendaService::meetingQuery(std::string userName, std::string title) {
  std::function<bool(const Meeting& m)> filterm = [userName, title](const Meeting& m)->bool{return ((userName == m.getParticipator() || userName == m.getSponsor()) && (m.getTitle() == title));};
  return (storage_->queryMeeting(filterm));
}
std::list<Meeting> AgendaService::meetingQuery(std::string userName, std::string startDate, std::string endDate) {
  Date sd = Date::stringToDate(startDate);
  Date ed = Date::stringToDate(endDate);
  std::function<bool(const Meeting& m)> filterm = [userName, sd, ed](const Meeting& m)->bool {
	  if (userName == m.getParticipator() || userName == m.getSponsor()) {
		  if ((sd >= m.getStartDate() && ed <= m.getEndDate()) || (sd <= m.getStartDate() && ed > m.getStartDate()) || (sd <= m.getEndDate() && ed >= m.getEndDate()))
			return true;
          }
            return false;
  };
  return (storage_->queryMeeting(filterm));
}
std::list<Meeting> AgendaService::listAllMeetings(std::string userName) {
  std::function<bool(const Meeting& m)> filterm = [userName](const Meeting& m)->bool{return (userName == m.getParticipator() || userName == m.getSponsor());};
  return (storage_->queryMeeting(filterm));
}
std::list<Meeting> AgendaService::listAllSponsorMeetings(std::string userName) {
  std::function<bool(const Meeting& m)> filterm = [userName](const Meeting& m)->bool{return (userName == m.getSponsor());};
  return (storage_->queryMeeting(filterm));
}
std::list<Meeting> AgendaService::listAllParticipateMeetings(std::string userName) {
  std::function<bool(const Meeting& m)> filterm = [userName](const Meeting& m)->bool{return (userName == m.getParticipator());};
  return (storage_->queryMeeting(filterm));
}
bool AgendaService::deleteMeeting(std::string userName, std::string title) {
  std::function<bool(const Meeting& m)> filterm = [userName, title](const Meeting& m)->bool{return (userName == m.getSponsor() && m.getTitle() == title);};
  if (storage_->queryMeeting(filterm).empty()) return false;
  storage_->deleteMeeting(filterm);
  return true;
}
bool AgendaService::deleteAllMeetings(std::string userName) {
  std::function<bool(const Meeting& m)> filterm = [userName](const Meeting& m)->bool{return userName == m.getSponsor();};
  if (storage_->queryMeeting(filterm).empty()) return false;
  storage_->deleteMeeting(filterm);
  return true;
}
void AgendaService::startAgenda(void) {
  std::cout << "------------------------ Agenda --------------------\n";
  std::cout << "Action :\n";
  std::cout << "l   - log in Agenda by user name and password\n";
  std::cout << "r   - register an Agenda account\n";
  std::cout << "q   - quit Agenda\n";
  std::cout << "----------------------------------------------------\n" << std::endl;
  std::cout << "Agenda : ~$ ";
}
AgendaService::~AgendaService() {
  storage_->sync();
}
void AgendaService::quitAgenda(void) {
}
