#include "Date.h"
#include <string>
#include <iostream>

Date::Date() {
}

Date::Date(int year,int month,int day,int hour,int minute) {
	year_ = year;
	month_ = month;
	day_ = day;
	hour_ = hour;
	minute_ = minute;
}

int Date::getYear(void) const {
	return year_;
} 

void Date::setYear(int year) {
	year_ = year;
}

int Date:: getMonth(void) const {
	return month_;
}

void Date::setMonth(int month) {
	month_ = month;
}

int Date::getDay(void) const {
	return day_;
}

void Date::setDay(int day) {
	day_ = day;
}

int Date::getHour(void) const {
	return hour_;
}

void Date::setHour(int hour) {
	hour_ = hour;
}

int Date::getMinute(void) const {
	return minute_;
}

void Date::setMinute(int minute) {
	minute_ = minute;
}

bool Date::isValid(Date date) {
	int day[13] = {29, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	if (date.getYear() < 1000 || date.getYear() > 9999) return false;
	if (date.getMonth() < 1 || date.getMonth() > 12) return  false;
	if (date.getDay() < 1 || date.getDay() > 31) return false;
    if (date.getHour() < 0 || date.getHour() >= 24) return false;
	if (date.getMinute() < 0 || date.getMinute() >= 60) return false;
	if (date.getMonth() != 2) {
		if (day[date.getMonth()] < date.getDay()) return false;
	}
	if (date.getMonth() == 2) {
		if (date.getYear() % 4  == 0 && date.getYear() % 100 != 0 || date.getYear() % 400 == 0) {
			if (date.getDay() > 29)
				return false;
		}
		else if (date.getDay() > 28) {
			return false;
		}
	}
	return true;
}

Date Date::stringToDate(std::string dateString) {
	Date t;
	int temp = (dateString[0]-'0')*1000;
	temp = temp+(dateString[1]-'0')*100;
	temp = temp+(dateString[2]-'0')*10;
	temp = temp+dateString[3]-'0';
	t.setYear(temp);
	temp = (dateString[5]-'0')*10;
	temp += dateString[6]-'0';
	t.setMonth(temp);
	temp = (dateString[8]-'0')*10;
	temp += dateString[9]-'0';
	t.setDay(temp);
	temp = (dateString[11]-'0')*10;
	temp += dateString[12]-'0';
	t.setHour(temp);
	temp = (dateString[14]-'0')*10;
	temp += dateString[15]-'0';
	t.setMinute(temp);
	return t;    
}

std::string Date::dateToString(Date date) {
	std::string temp;
	char cc1,cc2,cc3,cc4;
	cc1 = date.getYear() / 1000 + '0';
	temp = cc1;
	cc2 = date.getYear() / 100 - (cc1 - '0') * 10 + '0';
	temp += cc2;
	cc3 = date.getYear() / 10 - (cc1 - '0') * 100 - (cc2 - '0') * 10 + '0';
	temp += cc3;
	cc4 = date.getYear() - (cc1 - '0') * 1000 - (cc2 - '0') * 100 - (cc3 - '0') * 10 + '0';
	temp += cc4;
	temp += '-';
	if (date.getMonth() >= 10)
	{
		cc1 = date.getMonth() % 10 + '0';
		temp = temp + '1' + cc1;
	} else {
		cc1 = date.getMonth() + '0';
		temp += '0';
		temp += cc1;
	}
	temp += '-';
	if (date.getDay() >= 10 && date.getDay() < 20)
	{
		cc1 = date.getDay() % 10 + '0';
		temp = temp + '1' + cc1;
	}
	else if (date.getDay() >= 20 && date.getDay() < 30) {
		cc1 = date.getDay() % 10+ '0';
        temp = temp + '2' + cc1;
	}
	else if (date.getDay() >= 30) {
		cc1 = date.getDay() % 10 + '0';
		temp = temp + '3' + cc1;
	}
	else if (date.getDay() < 10)
	{
		cc1 = date.getDay() + '0';
		temp += '0';
		temp += cc1;
	}
	temp += '/';
	if (date.getHour() >= 10 && date.getHour() < 20)
	{
		cc1 = date.getHour() % 10 + '0';
		temp = temp + '1' + cc1;
	}
	else if (date.getHour() >= 20) {
		cc1 = date.getHour() % 10 + '0';
		temp = temp + '2' + cc1;
	}
	else if (date.getHour() < 10)
	{
		cc1 = date.getHour() + '0';
		temp += '0';
		temp += cc1;
	}
	temp += ':';
	if (date.getMinute() >= 10)
	{
		cc1 = date.getMinute() % 10 + '0';
		char z = '0' + (date.getMinute() / 10) % 10;
		temp = temp + z + cc1;
	}
	else
	{
		cc1 = date.getMinute() + '0';
		temp += '0';
		temp += cc1;
	}
	return temp;
} 

Date& Date::operator=(const Date& date) {
	year_ = date.getYear();
	month_ = date.getMonth();
	day_ = date.getDay();
	hour_ = date.getHour();
	minute_ = date.getMinute(); 
    return *this;
}

bool Date::operator==(const Date& date) const {
	if (year_ != date.getYear()) return false;
	if (month_ != date.getMonth()) return false;
	if (day_ != date.getDay()) return false;
	if (hour_ != date.getHour()) return false;
	if (minute_ != date.getMinute()) return false;
    return true;
}

bool Date::operator>(const Date& date) const
{
	if (year_ > date.getYear()) return true;
	else if (year_ == date.getYear() && month_ > date.getMonth()) return true;
	else if (year_ == date.getYear() && month_ == date.getMonth() && day_ > date.getDay()) return true;
	else if (year_ == date.getYear() && month_ == date.getMonth() && day_ == date.getDay() && hour_ > date.getHour()) return true;
	else if (year_ == date.getYear() && month_ == date.getMonth() && day_ == date.getDay() && hour_ == date.getHour() && minute_ > date.getMinute()) return true;
    return false;
}

bool Date::operator<(const Date& date) const
{
	if (year_ < date.getYear()) return true;
	else if (year_ == date.getYear() && month_ < date.getMonth()) return true;
	else if (year_ == date.getYear() && month_ == date.getMonth() && day_ < date.getDay()) return true;
	else if (year_ == date.getYear() && month_ == date.getMonth() && day_ == date.getDay() && hour_ < date.getHour()) return true;
	else if (year_ == date.getYear() && month_ == date.getMonth() && day_ == date.getDay() && hour_ == date.getHour() && minute_ < date.getMinute()) return true;
    return false;
}

bool Date::operator>=(const Date& date) const
{
	if (year_ > date.getYear()) return true;
	else if (year_ == date.getYear() && month_ > date.getMonth()) return true;
	else if (year_ == date.getYear() && month_ == date.getMonth() && day_ > date.getDay()) return true;
	else if (year_ == date.getYear() && month_ == date.getMonth() && day_ == date.getDay() && hour_ > date.getHour()) return true;
	else if (year_ == date.getYear() && month_ == date.getMonth() && day_ == date.getDay() && hour_ == date.getHour() && minute_ > date.getMinute()) return true;
	else if (year_ == date.getYear() && month_ == date.getMonth() && day_ == date.getDay() && hour_ == date.getHour() && minute_ == date.getMinute()) return true;
    return false;
}

bool Date::operator<=(const Date& date) const
{
	if(year_ < date.getYear()) return true;
	else if(year_ == date.getYear() && month_ < date.getMonth()) return true;
	else if(year_ == date.getYear() && month_ == date.getMonth() && day_ < date.getDay()) return true;
	else if (year_ == date.getYear() && month_ == date.getMonth() && day_ == date.getDay() && hour_ < date.getHour()) return true;
	else if (year_ == date.getYear() && month_ == date.getMonth() && day_ == date.getDay() && hour_ == date.getHour() && minute_ < date.getMinute()) return true;
	else if (year_ == date.getYear() && month_ == date.getMonth() && day_ == date.getDay() && hour_ == date.getHour() && minute_ == date.getMinute()) return true;
    return false;
}

