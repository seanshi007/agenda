#include"Storage.h"
#include<iostream>
#include<list>
#include<fstream>
#include<string>
#include<functional>
#include<cmath>
Storage* Storage::instance_ = NULL;
Storage* Storage::getInstance(void) {
  if (instance_) {
    return instance_;
  } else {
    instance_ = new Storage();
    return instance_;
  }
}
bool Storage::readFromFile(const char *fpath) {
	std::ifstream in(fpath);
	std::string numline = "";
	std::string userline = "";
	std::getline(in, numline);                                          //读入第一行
	if (numline == "") {
		return false;                                                   //若第一行为空返回false
	}
	int n = 0;
	int num1 = 0;                                                       //user计数器
	for (int i = 0; i < numline.length(); i++) {
		if (numline[i] - '0' >= 0 && numline[i] - '0' <= 9) {           //寻找第一行中数字
			n = n*10 + (numline[i]-'0');                                //计算出user数量
		}
	}
	while (num1 < n) {
		User u;
		std::string ans = "";
		int j = 0;
		int select = 1;                                                 //记录找到的是哪一元素
		std::getline(in, userline);
		while (userline[j] != '}') {
			if (userline[j] != '"') {
				j++;
			} else {                                                    //找到'"'号，然后用ans存储在下一次'"'号出现之间的元素
				j++;
				while (userline[j] != '"') {
					ans = ans + userline[j];
					j++;
				}
				if (select == 1) {
					u.setName(ans);
				}
				if (select == 2) {
					u.setPassword(ans);
				}
				if (select == 3) {
					u.setEmail(ans);
				}
				if (select == 4) {
					u.setPhone(ans);
				}
				j++;
				ans = "";
				select++;
			}
		}
		userList_.push_back(u);
		num1++;
	}
	numline = "";
	n = 0;
	int num2 = 0;
	std::string meetingline = "";
	std::getline(in, numline);
	for (int i = 0; i < numline.length(); i++) {
		if (numline[i] - '0' >= 0 && numline[i] - '0' <= 9) {
			n = n*10 + (numline[i]-'0');
		}
	}
	while (num2 < n) {
		Meeting m;
		std::string ans = "";
		int j = 0;
		int select = 1;
		std::getline(in, meetingline);
		while (meetingline[j] != '}') {
			if (meetingline[j] != '"') {
				j++;
			} else {
				j++;
				while (meetingline[j] != '"') {
					ans = ans + meetingline[j];
					j++;
				}
				if (select == 1) {
					m.setSponsor(ans);
				}
				if (select == 2) {
					m.setParticipator(ans);
				}
				if (select == 3) {
					m.setStartDate(Date::stringToDate(ans));
				}
				if (select == 4) {
					m.setEndDate(Date::stringToDate(ans));
				}
				if (select == 5) {
					m.setTitle(ans);
				}
				j++;
				ans = "";
				select++;
			}
		}
		meetingList_.push_back(m);
		num2++;
	}
	return true;
}
bool Storage::writeToFile(const char *fpath) {
  std::ofstream out(fpath);
  out << "{collection:\"User\",total:" << userList_.size() << "}" << std::endl;
  std::list<User>::iterator p;
  std::list<Meeting>::iterator q;
  for (p = userList_.begin(); p != userList_.end(); p++) {
    out << "{name:\"" << p->getName() << "\",password:\""
    << p->getPassword() << "\",email:\"" << p->getEmail() <<
    "\",phone:\"" << p->getPhone() << "\"}" << std::endl;
  }
  out << "{collection:\"Meeting\",total:" << meetingList_.size()
  << "}" << std::endl;
  for (q = meetingList_.begin(); q != meetingList_.end(); q++) {
    out << "{sponsor:\"" << q->getSponsor() << "\",participator:\""
    << q->getParticipator() << "\",sdate:\"" <<
    Date::dateToString(q->getStartDate()) << "\",edate:\""
    << Date::dateToString(q->getEndDate()) << "\",title:\""
    << q->getTitle() << "\"}" << std::endl;
  }
  out.close();
  return true;
}
Storage::Storage() {
  readFromFile("agenda.data");
}
void Storage::createMeeting(const Meeting& m) {
  meetingList_.push_back(m);
}
Storage::~Storage() {
  instance_ = NULL;
}
bool Storage::sync(void) {
  return writeToFile("agenda.data");
}
void Storage::createUser(const User& u) {
  userList_.push_back(u);
}
std::list<User> Storage::queryUser(std::function<bool(const User&)> filter) {
  std::list<User>::iterator p;
  std::list<User> temp;
  for (p = userList_.begin(); p != userList_.end(); p++) {
    if (filter(*p)) {
      temp.push_back(*p);
    }
  }
  return temp;
}
int Storage::updateUser(std::function<bool(const User&)>filter, std::function<void(User&)> switcher) {
  int num = 0;
  std::list<User>::iterator p;
  for (p = userList_.begin(); p != userList_.end(); p++) {
    if (filter(*p)) {
      switcher(*p);
      num++;
    }
  }
  return num;
}
int Storage::deleteUser(std::function<bool(const User&)> filter) {
  int num = 0;
  std::list<User>::iterator p;
  for (p = userList_.begin(); p != userList_.end();) {
    if (filter(*p)) {
      p = userList_.erase(p);
      num++;
    } else {
      ++p;
    }
  }
  return num;
}
std::list<Meeting> Storage::queryMeeting(std::function<bool(const Meeting&)> filter) {
  std::list<Meeting>::iterator p;
  std::list<Meeting> temp;
  for (p = meetingList_.begin(); p != meetingList_.end(); p++) {
    if (filter(*p)) {
      temp.push_back(*p);
    }
  }
  return temp;
}
int Storage::updateMeeting(std::function<bool(const Meeting&)> filter, std::function<void(Meeting&)> switcher) {
  int num = 0;
  std::list<Meeting>::iterator p;
  for (p = meetingList_.begin(); p != meetingList_.end(); p++) {
    if (filter(*p)) {
      switcher(*p);
      num++;
    }
  }
  return num;
}
int Storage::deleteMeeting(std::function<bool(const Meeting&)> filter) {
  int num = 0;
  std::list<Meeting>::iterator p;
  for (p = meetingList_.begin(); p != meetingList_.end();) {
    if (filter(*p)) {
      p = meetingList_.erase(p);
      num++;
    } else {
      ++p;
    }
  }
  return num;
}
